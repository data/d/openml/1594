# OpenML dataset: news20

https://www.openml.org/d/1594

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Ken Lang
**Source**: [original](http://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/multiclass.html) - Date unknown  
**Please cite**: Ken Lang.
Newsweeder: Learning to filter netnews.
In Proceedings of the Twelfth International Conference on Machine Learning, pages 331-339, 1995.  

#Dataset from the LIBSVM data repository.

Preprocessing: First 80/20 training/testing split. Also see http://qwone.com/~jason/20Newsgroups/

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1594) of an [OpenML dataset](https://www.openml.org/d/1594). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1594/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1594/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1594/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

